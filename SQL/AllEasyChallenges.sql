/*All EASY challenges*/

/*Revising the Select Query I ------By adding the  *, all fields in that table will return that meets the WHERE criteria */
SELECT *
FROM CITY
WHERE POPULATION >= 100000 AND COUNTRYCODE = 'USA'

/*Revising the Select Query II ------Pulling all records that meet the WHERE criteria but only listing the NAME field for each row. */
SELECT NAME
FROM CITY
WHERE POPULATION >= 120000 AND COUNTRYCODE = 'USA'

/* SELECT ALL ---- By adding the  *, all fields in that table will return */
SELECT *
FROM CITY

/* SELECT ALL BY ID ---- By adding the  *, all fields in that table will return that has the same ID. */
SELECT *
FROM CITY
WHERE ID=1661

/*Japanese city attributes */
SELECT *
FROM CITY
WHERE COUNTRYCODE = 'jpn'

/*Weather Observation 1*/
SELECT CITY, STATE
FROM STATION

/*Weather Observation 2*/
SELECT ROUND(SUM(lAT_N),2),' ',ROUND(SUM(LONG_W),2) 
FROM STATION

/*Weather Observation 3  ---- The attribute 'Distinct' will return unique results specific to that field*/
SELECT DISTINCT(CITY)
FROM STATION
WHERE ID % 2 = 0

/*Weather Observation 4 ---- the Count attribute will could the number of records it returns. */
select count(city)- count(distinct(city))
from station

/*Weather Observation 5 ---- 2 QUERIES. The attribute returns the character length in the field (). In case case, the length of the city name.*/
SELECT city, length(city)
FROM station
ORDER BY LENGTH(city), city asc 
LIMIT 1;

SELECT city, length(city)
FROM station
ORDER BY LENGTH(city) desc
LIMIT 1;

/*Weather Observation 6  --- the attribute in the WHERE clause RLIKE operates the same as the LIKE clause. This query is looking for cities that start with a vowel*/
SELECT DISTINCT CITY
FROM STATION
WHERE CITY RLIKE '^[aeiouAEIOU]';

/*Weather Observation 7  --- The WHERE attribute REGEXP stands for regular expression. This is looking at the city names ending with a vowel*/
SELECT DISTINCT CITY
FROM STATION
WHERE CITY REGEXP '[AEIOU]$'

/*Weather Observation 8 -- Listing cities that have vowels as both their first and last character*/
SELECT DISTINCT CITY
FROM STATION
WHERE CITY RLIKE '^[aeiou].*[aeiou]$'

/*Weather Observation 9  ---Listing cities that do not start with vowels*/
SELECT DISTINCT CITY
FROM STATION
WHERE CITY REGEXP '^[^aeiou]';

/*Weather Observation 10 --- Listing cities that do not end in vowels. Pay close attention to the ^ in the bracket*/
SELECT  DISTINCT CITY
FROM STATION
WHERE CITY REGEXP '[^aeiou]$';

/*Weather Observation 11  --- This query lists cities that do not start or end with vowels. The important part here is the  | or pipe*/
SELECT DISTINCT CITY
FROM STATION
WHERE CITY REGEXP '^[^aeiou]|[^aeiou]$';

/*Weather Observation 12  --- This query lists cities that do not start  AND do not end with a vowel*/
SELECT DISTINCT city 
FROM station 
WHERE city regexp '^[^aeiou].*[^aeiou]$'



/*Higher than 75 Marks */
SELECT NAME
FROM STUDENTS
WHERE MARKS > 75
ORDER BY SUBSTR(NAME, -3), ID ASC;

/*Employee Names */
SELECT NAME
FROM EMPLOYEE 
ORDER BY NAME ASC

/*Employee Salaries */
SELECT NAME
FROM EMPLOYEE
WHERE SALARY > 2000 AND MONTHS <10
ORDER BY EMPLOYEE_ID ASC


/* Type of Triangle */
SELECT 
    CASE 
        WHEN (A + B <= C) OR (A + C <= B) OR (B + C < A) 
            THEN 'Not A Triangle'
        WHEN (A = B) AND (A = C) 
            THEN 'Equilateral'
        WHEN (A != B ) AND (A != C) AND (B != C) 
            THEN 'Scalene'
        ELSE 'Isosceles'
    END
FROM TRIANGLES


